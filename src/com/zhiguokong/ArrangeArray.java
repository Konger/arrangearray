package com.zhiguokong;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/*
Implement a method that moves all 0’s in an integer array to the end of the array. Non-zero elements are shifted to the beginning, without changing their relative order. 
Input: int val, int[] a. 
Output int[] a. 
Please assume that the array can be of an arbitrary and large length (n). 
Also, 0 is given as an example of an input parameter named val. The method should work with any other passed integer value.
*/


public class ArrangeArray {
	
	private ArrayList<Integer> arrayList;
	
	//Constructors
	public ArrangeArray( int[] ray ) {
		if( ray != null) {
			this.arrayList = new ArrayList<Integer>();
			for(int element : ray)
				this.arrayList.add( (Integer)element );
		}
		
	}
	
	public ArrangeArray( int arg ) {
		this.arrayList = new ArrayList<Integer>();
		this.arrayList.add( (Integer)arg );
	}
	
	public static void main(String[] args) {	
		int[] a= { 5, 7, 4, 0, 2, 0, 1 };
		ArrangeArray arrangeArray = new ArrangeArray(a);
		a = arrangeArray.shiftZeroRight();
		System.out.print(Arrays.toString(a));
		
	}
	
	public int[] shiftZeroRight ( ) {
		int pivot = findFirstNonZeroElement(0);
		if(arrayList == null ) {
			return null;
		}
		else if(arrayList.size() ==1 || pivot==-1)
			return convertToArray();
		else {
			for(int i = 0; i <arrayList.size(); i++) {
				Integer element = arrayList.get(i);
				if( element==0 ) {
					pivot = findFirstNonZeroElement(i+1);
					if( pivot!=-1 ) { //shift zero element to the right
						arrayList.set(i, arrayList.get(pivot) );
						Integer zero = new Integer(0);
						arrayList.set(pivot, zero );
					}
				}
			}//end of for
			return convertToArray();
		}		
	}
	
	private int findFirstNonZeroElement(int start) {
		if(this.arrayList == null)
			return -1;
		for(int i= start; i< arrayList.size(); i++) {
			if(arrayList.get(i)!=0)
				return i;
		}
		return -1;
	}
	
	private int[] convertToArray() {
		int[] result = new int[arrayList.size()];
		Iterator<Integer> iter = arrayList.iterator();
		for(int i=0; i< result.length; i++)
			result[i]= iter.next().intValue();
		return result;
	}
		
}
