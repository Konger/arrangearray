package com.zhiguokong;


import junit.framework.Assert;

import org.junit.Test;

public class ArrangeArrayTest {

	@Test
	public void testNull() {
		int[] ray = null;
		ArrangeArray arrangeArray = new ArrangeArray(ray);
		Assert.assertNull("failure - expected result to be null", arrangeArray.shiftZeroRight());
	}
	
	
	@Test
	public void testAllZeroArray() {
		int[] ray = { 0, 0, 0, 0 };
		ArrangeArray arrangeArray = new ArrangeArray(ray);
		int[] result = arrangeArray.shiftZeroRight();
		for(int i = 0; i< result.length; i++ )
			Assert.assertEquals("failure - expected result match", ray[i], result[i]);
	}
	
	@Test
	public void testNoZeroArray() {
		int[] ray = { 5, 1, 2, 3 };
		ArrangeArray arrangeArray = new ArrangeArray(ray);
		int[] result = arrangeArray.shiftZeroRight();
		for(int i = 0; i< result.length; i++ )
			Assert.assertEquals("failure - expected result match", ray[i], result[i]);
	}
	
	@Test
	public void testZeroStartArray() {
		int[] ray = { 0, 1, 2, 3 };
		int[] expectedResult = { 1, 2, 3, 0 };
		ArrangeArray arrangeArray = new ArrangeArray(ray);
		int[] result = arrangeArray.shiftZeroRight();
		for( int i = 0; i< expectedResult.length; i++)
			Assert.assertEquals("failure - expected result match", expectedResult[i], result[i] );
	}
	
}
