# README #


### What is this repository for? ###

* Quick summary
Implement a method that moves all 0’s in an integer array to the end of the array. Non-zero elements are shifted to the beginning, without changing their relative order. 
Input: int val, int[] a. 
Output int[] a. 
Please assume that the array can be of an arbitrary and large length (n). 
Also, 0 is given as an example of an input parameter named val. The method should work with any other passed integer value.

* Version: 1.0
* 
### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions